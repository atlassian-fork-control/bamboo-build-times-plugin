<head>
    [#if isJob]
        <meta name="tab" content="jobBuildTimes"/>
    [#else]
        <meta name="tab" content="chainBuildTimes"/>
    [/#if]
</head>
<body>
<script src="${req.contextPath}/download/resources/com.atlassian.bamboo.plugin.bamboo-build-times-plugin:build-times-resources/d3.v5.min.js"></script>
<h1>Build Times</h1>
<div id="chartContainer">

</div>

    <script type="text/javascript">
        (function(){
            var buildKey = "${buildKey}";
            var buildNumber = "${buildNumber}";

            //width of the svg canvas
            var canvasWidth = AJS.$("#chartContainer").width();

            //width of the chart. This allows some overflow room for text
            var width = canvasWidth*0.8;
            var height = 30;
            var rectPadding = 3;
            var transitionDuration = 500;
            var legendHeight = 50;

            var labelOffset = height/2 + 5;
            var colorFillMap = {"green":"#393", "red":"#A00", "vcs": "#24A", "not built":"#666", "pending":"#3c78b5", "queued":"#3c78b5", "current":"#3c78b5"};
            var tickLineColor = "#ccc";

            var pollTime = setPollTimeForJobs(1);

            function setPollTimeForJobs(jobs) {
                // Increases polling time based on number of jobs that need to be rendered
                var basePollTime = 3000;
                var perJobExtraPollTime = 1000;
                pollTime = ((jobs - 1) * perJobExtraPollTime) + basePollTime;
            }

            function positionRectHeight(i) {
                return i*(height+rectPadding);
            }

            function totalWaitTime(job) {
                return job.queueTime[0] + job.preQueueTime[0];
            }

            function makeId(prefix, i) {
                return prefix.toString() + "-job-" + i.toString();
            }

            function makeShadow(withColor) {
                // this shadow has a dark underline, but additionally is also drawn around with a given color
                return "text-shadow:1px 1px 1px rgba(0, 0, 0, 0.5)"
                        + ", 1px 1px 1px " + withColor
                        + ", -1px 1px 1px " + withColor
                        + ", 1px -1px 1px " + withColor
                        + ", -1px -1px 1px " + withColor + ";";
            }

            function getLabelPosition(job, i, x) {
                return x(job.totalDuration) + 20;
            }

            function makeAgentName(job) {
                var message = job.jobName + " built on " + job.agentName;
                if(job.result == "pending") {
                    message = job.jobName + " is pending";
                }
                else if (job.result == "queued") {
                    message = job.jobName + " is queued";
                }
                else if (job.result == "not built") {
                    message = job.jobName + " was not built";
                }
                else if (job.result == "current") {
                    message = job.jobName + " is building on " + job.agentName;
                }

                if(job.rerun == "true") {
                    message = message + " (rebuilt)";
                }
                return message;
            }

            function formatTime(timeInSeconds) {
                var seconds = (timeInSeconds % 60);
                var minutes = (Math.floor(timeInSeconds / 60) % 60);
                var hours = Math.floor(timeInSeconds / (60*60));

                //left-pads a number with zero if it's 1 digit long
                function pad(number) { return (number < 10 ? "0" + number : "" + number); }

                //show the largest non-zero unit and all smaller units
                return (
                    (hours ? pad(hours) + ":" : "") +
                    (hours || minutes ? pad(minutes) + ":" : "") +
                    pad(seconds));
            }

            function redraw(data) {
                var totalDurations = AJS.$.map(data.jobs, function(element) { return element.totalDuration; });
                var chartHeight = (height + 2*rectPadding) * data.jobs.length;

                totalDurations.push(data.averageBuildTime);

                var x = d3.scaleLinear()
                        .domain([0, d3.max(totalDurations)])
                        .range([0, width]);

                var chart = d3.select("#chart");

                //Ticklines
                var tickLines = chart.selectAll("line.tickLine")
                        .data(x.ticks(20));

                //Append new
                tickLines.enter().insert("line", "rect.buildRect, rect.vcsRect")
                        .attr("x1", x)
                        .attr("x2", x)
                        .attr("y1", 0)
                        .attr("y2", chartHeight)
                        .attr("class", "tickLine")
                        .style("stroke", tickLineColor);

                //Update
                tickLines.transition().duration(transitionDuration)
                        .attr("x1", x)
                        .attr("x2", x);

                //Remove extra
                tickLines.exit().remove();

                chart.selectAll("line.queueLine")
                        .data(data.jobs)
                        .transition().duration(transitionDuration)
                        .attr("x2", function(d,i) {return x(totalWaitTime(d));});

                //Box representing build duration
                chart.selectAll("rect.buildRect")
                        .data(data.jobs)
                        .attr("style", function(d, i){return "fill:" + colorFillMap[d.result];})
                        .transition().duration(transitionDuration)
                        .attr("y", function(d, i) { return positionRectHeight(i)})
                        .attr("id", function(d, i) { return makeId("buildRect", i);})
                        .attr("class", "buildRect")
                        .attr("width", function(d) {return x(d.buildingTime);})
                        .attr("height", height)
                        .attr("x", function(d) { return x(totalWaitTime(d)) + x(d.vcsUpdatingTime[0]);});

                //Box representing checkout time (blue)
                chart.selectAll("rect.vcsRect")
                        .data(data.jobs)
                        .transition().duration(transitionDuration)
                        .attr("y", function(d, i) { return positionRectHeight(i)})
                        .attr("id", function(d, i) { return makeId("vcsRect", i);})
                        .attr("class", "vcsRect")
                        .attr("width", function(d) {return x(d.vcsUpdatingTime[0]);})
                        .attr("height", height)
                        .attr("style", function(d,i){return "fill:" + colorFillMap["vcs"];})
                        .attr("x", function(d) { return x(totalWaitTime(d));});

                //Text with build duration
                chart.selectAll("text.buildTimeText")
                        .data(data.jobs)
                        .transition().duration(transitionDuration)
                        .attr("y", function(d, i) { return positionRectHeight(i)})
                        .attr("text-anchor", "end")
                        .attr("dy", labelOffset + "px")
                        .attr("class", "buildTimeText")
                        .attr("x", function(d) {return x(totalWaitTime(d)) + x(d.duration) - 3;})
                        .attr("style", function(d) { return "fill:white;" + makeShadow(colorFillMap[d.result]); })
                        .attr("id", function(d, i) { return makeId("buildTimeText", i);})
                        .text(function(d) { return formatTime(d.buildingTime); });

                //Only show it if the corresponding box is large enough
                chart.selectAll("text.buildTimeText")
                        .attr("opacity", function(d,i) {
                            if(d3.select("#" + makeId("buildRect", i)).attr("width") <= 6 + this.getBBox().width) {
                                return "0";
                            }
                            return "1";
                        });

                //Text with vcs updating duration
                chart.selectAll("text.vcsTimeText")
                        .data(data.jobs)
                        .transition().duration(transitionDuration)
                        .attr("y", function(d,i) { return positionRectHeight(i)})
                        .attr("text-anchor", "end")
                        .attr("dy", labelOffset + "px")
                        .attr("class", "vcsTimeText")
                        .attr("x", function(d) {return x(totalWaitTime(d)) + x(d.vcsUpdatingTime) -3;})
                        .attr("style", "fill:white;" + makeShadow(colorFillMap["vcs"]))
                        .attr("id", function(d, i) { return makeId("vcsTimeText", i);})
                        .text(function(d) { return formatTime(d.vcsUpdatingTime); });

                //Only show it if the corresponding box is large enough
                chart.selectAll("text.vcsTimeText")
                        .attr("opacity", function(d,i) {
                            if(d3.select("#" + makeId("vcsRect", i)).attr("width") <= 6 + this.getBBox().width) {
                                return "0";
                            }
                            return "1";
                        });

                //Scale number on the ticks
                var ticks = chart.selectAll("text.tick")
                        .data(x.ticks(20));

                ticks.enter().append("text")
                        .attr("class", "tick")
                        .attr("x", x)
                        .attr("y", -5)
                        .attr("text-anchor", "middle")
                        .text(function(d) {return formatTime(d.valueOf());});

                ticks.transition().duration(transitionDuration)
                        .attr("x", x)
                        .text(function(d) {return formatTime(d.valueOf());});

                ticks.exit().remove();

                //Background box for job and agent name, starts off invisible
                chart.selectAll("rect.jobListItem")
                        .data(data.jobs)
                        .transition().duration(transitionDuration)
                        .attr("x", function(d, i) {return getLabelPosition(d,i,x) - 5});

                //Text with job and agent name
                chart.selectAll("text.jobListItem")
                        .data(data.jobs)
                        .transition().duration(transitionDuration)
                        .attr("x", function(d,i) {return getLabelPosition(d,i,x);});
            }

            function drawLegend() {
                var legend = d3.select("#chartContainer").append("svg")
                        .attr("class", "chart")
                        .attr("id", "legend")
                        .attr("width", canvasWidth)
                        .attr("height", legendHeight)
                        .attr("style", "padding-top: 35px; padding-left: 35px; padding-bottom: 15px");

                var styles = ["green", "red", "vcs", "pending"];
                var labels = {"green":"build time (successful)", "red":"build time (failed)", "vcs": "version control checkout time", "pending":"currently building time"};

                //Draw Legend
                legend.append("rect")
                        .attr("y", 0)
                        .attr("x", 0)
                        .attr("width", 1310)
                        .attr("height", legendHeight)
                        .attr("style", "fill:#fff; stroke:#000");

                legend.selectAll("rect.legend")
                        .data(styles)
                        .enter().append("rect")
                        .attr("y", 10)
                        .attr("width", 60)
                        .attr("height", height)
                        .attr("style", function(d, i){return "fill:" + colorFillMap[d];})
                        .attr("x", function(d, i) {return 10 + 260*i});

                legend.selectAll("text.legend")
                        .data(styles)
                        .enter().append("text")
                        .attr("y", 10 + height/2 + 4)
                        .attr("x", function(d, i) {return 75 + 260*i})
                        .attr("text-anchor", "start")
                        .text(function(d, i) {return labels[d];});

                legend.append("line")
                        .attr("x1", function(d, i) {return 10 + 4*260;})
                        .attr("x2", function(d, i) {return 70 + 4*260;})
                        .attr("y1", legendHeight/2)
                        .attr("y2", legendHeight/2)
                        .style("stroke", "#000");

                legend.append("text")
                        .attr("y", 10 + height/2 + 4)
                        .attr("x", function(d, i) {return 75 + 4*260;})
                        .attr("text-anchor", "start")
                        .text("queue time");
            }

            function drawInitial(data){
                var totalDurations = AJS.$.map(data.jobs, function(element) { return element.totalDuration; });
                var chartHeight = (height + 2*rectPadding) * data.jobs.length;
                setPollTimeForJobs(data.jobs.length);

                totalDurations.push(data.averageBuildTime);

                drawLegend();

                var chart = d3.select("#chartContainer").append("svg")
                        .attr("class", "chart")
                        .attr("id", "chart")
                        .attr("width", canvasWidth)
                        .attr("height", chartHeight)
                        .attr("style", "padding: 20px; padding-left: 35px; padding-top: 50px");

                var x = d3.scaleLinear()
                        .domain([0, d3.max(totalDurations)])
                        .range([0, width]);

                //Ticklines
                chart.selectAll("line.tickLine")
                        .data(x.ticks(20))
                        .enter().append("line")
                        .attr("x1", x)
                        .attr("x2", x)
                        .attr("y1", 0)
                        .attr("y2", chartHeight)
                        .attr("class", "tickLine")
                        .style("stroke", tickLineColor);

                //Solid line at the start
                chart.append("line")
                        .attr("y1", 0)
                        .attr("y2", chartHeight)
                        .style("stroke", "#000");

                //Units
                chart.append("text")
                        .attr("x", width/2)
                        .attr("y", -28)
                        .attr("text-anchor", "middle")
                        .attr("style", "font-size: 16");

                chart.selectAll("line.queueLine")
                        .data(data.jobs)
                        .enter().append("line")
                        .attr("x1", 0)
                        .attr("x2", function(d,i) {return x(totalWaitTime(d));})
                        .attr("y1", function(d,i) { return positionRectHeight(i) + height/2})
                        .attr("y2", function(d,i) { return positionRectHeight(i) + height/2})
                        .attr("class", "queueLine")
                        .style("stroke", "#000");

                //Box representing build duration
                chart.selectAll("rect.buildRect")
                        .data(data.jobs)
                        .enter().append("rect")
                        .attr("y", function(d,i) { return positionRectHeight(i)})
                        .attr("id", function(d, i) { return makeId("buildRect", i);})
                        .attr("class", "buildRect")
                        .attr("width", function(d) {return x(d.buildingTime);})
                        .attr("height", height)
                        .attr("style", function(d,i){return "fill:" + colorFillMap[d.result];})
                        .attr("x", function(d) { return x(totalWaitTime(d)) + x(d.vcsUpdatingTime[0]);})
                        .append("title")
                        .text(function(d) {return makeAgentName(d);});

                //Box representing checkout time (blue)
                chart.selectAll("rect.vcsRect")
                        .data(data.jobs)
                        .enter().append("rect")
                        .attr("y", function(d,i) { return positionRectHeight(i)})
                        .attr("id", function(d, i) { return makeId("vcsRect", i);})
                        .attr("class", "vcsRect")
                        .attr("width", function(d) {return x(d.vcsUpdatingTime[0]);})
                        .attr("height", height)
                        .attr("style", function(d,i){return "fill:" + colorFillMap["vcs"];})
                        .attr("x", function(d) { return x(totalWaitTime(d));});

                //Text with build duration
                chart.selectAll("text.buildTimeText")
                        .data(data.jobs)
                        .enter().append("text")
                        .attr("y", function(d,i) { return positionRectHeight(i)})
                        .attr("text-anchor", "end")
                        .attr("dy", labelOffset + "px")
                        .attr("class", "buildTimeText")
                        .attr("x", function(d) {return x(totalWaitTime(d)) + x(d.duration) -3;})
                        .attr("style", function(d) { return "fill:white;" + makeShadow(colorFillMap[d.result]);})
                        .attr("id", function(d, i) { return makeId("buildTimeText", i);})
                        .attr("opacity", "0")
                        .text(function(d) { return formatTime(d.buildingTime); });

                //Only show it if the corresponding box is large enough
                chart.selectAll("text.buildTimeText")
                        .attr("opacity", function(d,i) {
                            if(d3.select("#" + makeId("buildRect", i)).attr("width") <= 6 + this.getBBox().width) {
                                return "0";
                            }
                            return "1";
                        });

                //Text with vcs updating duration
                chart.selectAll("text.vcsTimeText")
                        .data(data.jobs)
                        .enter().append("text")
                        .attr("y", function(d,i) { return positionRectHeight(i)})
                        .attr("text-anchor", "end")
                        .attr("dy", labelOffset + "px")
                        .attr("class", "vcsTimeText")
                        .attr("x", function(d) {return x(totalWaitTime(d)) + x(d.vcsUpdatingTime) -3;})
                        .attr("style", "fill:white;" + makeShadow(colorFillMap["vcs"]))
                        .attr("id", function(d, i) { return makeId("vcsTimeText", i);})
                        .attr("opacity", "0")
                        .text(function(d) { return formatTime(d.vcsUpdatingTime); });

                //Only show it if the corresponding box is large enough
                chart.selectAll("text.vcsTimeText")
                        .attr("opacity", function(d,i) {
                            if(d3.select("#" + makeId("vcsRect", i)).attr("width") <= 6 + this.getBBox().width) {
                                return "0";
                            }
                            return "1";
                        });

                //Scale number on the ticks
                chart.selectAll("text.tick")
                        .data(x.ticks(20))
                        .enter().append("text")
                        .attr("class", "tick")
                        .attr("x", x)
                        .attr("y", -5)
                        .attr("text-anchor", "middle")
                        .text(function(d) {return formatTime(d.valueOf());});

                //Background box for job and agent name, starts off invisible
                chart.selectAll("rect.jobListItem")
                        .data(data.jobs)
                        .enter().append("rect")
                        .attr("x", function(d, i) {return getLabelPosition(d,i,x) - 5})
                        .attr("y", function(d,i) {return positionRectHeight(i);})
                        .attr("height", height)
                        .attr("width", 0)
                        .attr("class", "jobListItem")
                        .attr("style", function(d,i){return "fill:#fff";})
                        .attr("opacity", "0.7")
                        .attr("id", function(d,i) {return makeId("jobListRect", i);});

                //Text with job and agent name
                chart.selectAll("text.jobListItem")
                        .data(data.jobs)
                        .enter().append("text")
                        .attr("x", function(d,i) {return getLabelPosition(d,i,x);})
                        .attr("y", function(d,i) {return positionRectHeight(i) + height - 12;})
                        .attr("text-anchor", "start")
                        .attr("class", "jobListItem")
                        .attr("id", function(d,i) {return makeId("jobListItem", i);})
                        .text(function(d) {return makeAgentName(d);});

                //Resize the background boxes now that we know how wide the text is
                chart.selectAll("rect.jobListItem")
                        .attr("width", function(d,i) { return d3.select("#" + makeId("jobListItem", i)).node().getBBox().width + 10});
            }
            d3.json("/bamboo/build/result/getTimesDataJson.action?buildKey="+buildKey+"&buildNumber="+buildNumber).then(function(data) {
                drawInitial(data);
                setInterval(function() {
                    d3.json("/bamboo/build/result/getTimesDataJson.action?buildKey="+buildKey+"&buildNumber="+buildNumber).then(redraw);
                }, pollTime);
            });

        })();

    </script>

</body>
